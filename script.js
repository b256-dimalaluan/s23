let trainer = {

	name: 'Ash Ketchum',
	age: 12,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	Friends: {
		hoen: ['May', 'Max'], 
		kanto: ['Brock', 'Misty'],
	},



	talk: function() {
		console.log("Pikachu! I choose you!");
	}
	
}

console.log(trainer)


console.log('Result from dot notation: ');
console.log(trainer.name);
console.log('Result from square bracket notation:');
console.log(trainer['pokemon']);
console.log('Result of talk method:');
trainer.talk();


function Pokemon(name, level, health, attack){
		this.name = name;
		this.level = level;
		this.health = level * 2;
		this.attack = level * 3;

	};

let pikachu = new Pokemon('Pikachu', 12); 
let geodude = new Pokemon('Geodude', 8 );
let mewtwo = new Pokemon('Mewtwo', 100);
let zapdos = new Pokemon('Zapdos', 100);

 console.log(zapdos);
 console.log(mewtwo);
 console.log(geodude);
 console.log(pikachu);



Pokemon.tackle = function(usedPokemon, targetPokemon) {
	console.log(usedPokemon.name + " tackled " + targetPokemon.name);

	let hpLeft = targetPokemon.health - usedPokemon.attack;

	let faint = function() {
		console.log(targetPokemon.name + " has fainted");
	};

	if(hpLeft <= 0) {
		console.log(faint());
	}

	else {
		console.log(targetPokemon.name + "'s health is now reduced to " + hpLeft);
	}
};





